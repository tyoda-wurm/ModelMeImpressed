/*
ModelMeImpressed mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License v3 as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.modelmeimpressed;

import com.wurmonline.server.behaviours.Action;
import com.wurmonline.server.behaviours.ActionEntry;
import com.wurmonline.server.behaviours.Actions;
import com.wurmonline.server.creatures.Creature;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import com.wurmonline.server.questions.BrowseModelsQuestion;
import org.gotti.wurmunlimited.modsupport.actions.ActionPerformer;
import org.gotti.wurmunlimited.modsupport.actions.ModAction;
import org.gotti.wurmunlimited.modsupport.actions.ModActions;

public class BrowseModelsAction implements ModAction, ActionPerformer {
    private final short actionId = (short)ModActions.getNextActionId();
    private final ActionEntry actionEntry;

    public BrowseModelsAction() {
        this.actionEntry = ActionEntry.createEntry(this.actionId, "Browse", "browsing models", new int[]{Actions.ACTION_TYPE_IGNORERANGE});
        ModActions.registerAction(this.actionEntry);
    }

    public short getActionId() {
        return this.actionId;
    }

    public ActionEntry getActionEntry() {
        return this.actionEntry;
    }

    public boolean action(Action action, Creature performer, Item source, Creature target, short num, float counter) {
        return this.action(action, performer, target, num, counter);
    }

    public boolean action(Action action, Creature performer, Creature target, short num, float counter) {
        return this.makeWindow(performer);
    }

    public boolean action(Action action, Creature performer, Item source, Item target, short num, float counter) {
        return this.action(action, performer, target, num, counter);
    }

    public boolean action(Action action, Creature performer, Item target, short num, float counter) {
        return this.makeWindow(performer);
    }

    private boolean makeWindow(Creature performer) {
        if (performer instanceof Player && performer.getPower() >= ModelMeImpressed.options.getGmPowerNeeded()) {
            BrowseModelsQuestion q = new BrowseModelsQuestion(performer, "Browse Models", "No questions.");
            q.sendQuestion();
        }
        return true;
    }
}
